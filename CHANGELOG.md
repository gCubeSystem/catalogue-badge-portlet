
# Changelog for Catalogue Badge Portlet

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v1.1.2] - 2023-02-13

- bug fix

## [v1.1.1] - 2021-10-13

- Feature #22177, Modify Catalogue widget portlet to display the types and the number when in root VO


## [v1.0.0] - 2017-12-06

First Release
