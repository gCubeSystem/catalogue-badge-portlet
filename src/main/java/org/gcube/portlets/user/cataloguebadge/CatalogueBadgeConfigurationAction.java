package org.gcube.portlets.user.cataloguebadge;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletPreferences;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;

public class CatalogueBadgeConfigurationAction extends DefaultConfigurationAction {
	private static Log _log = LogFactoryUtil.getLog(CatalogueBadgeConfigurationAction.class);
    @Override
    public void processAction(
        PortletConfig portletConfig, ActionRequest actionRequest,
        ActionResponse actionResponse) throws Exception {  

        super.processAction(portletConfig, actionRequest, actionResponse);

        PortletPreferences prefs = actionRequest.getPreferences();
        String catalogueURL = prefs.getValue("catalogueURL", "true");
      
    
        _log.debug("catalogueURL = " + catalogueURL + " in CatalogueBadgeConfigurationAction.processAction().");
       }
    
    @Override
    public String render(PortletConfig portletConfig,
            RenderRequest renderRequest, RenderResponse renderResponse)
            throws Exception {
 
        return "/WEB-INF/jsp/Catalogue-badge-portlet/config.jsp";
    }
 
}