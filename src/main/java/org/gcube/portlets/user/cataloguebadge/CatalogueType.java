package org.gcube.portlets.user.cataloguebadge;

public class CatalogueType implements Comparable<CatalogueType>{
	private String name;
	private String img;
	private String link;
	private int occurrence;

	public CatalogueType(String name, String img, String link, int occurrence) {
		super();
		this.name = name;
		this.img = img;
		this.link = link;
		this.occurrence = occurrence;
	}
	
	public String getName() {
		return name;
	}

	public String getImg() {
		return img;
	}

	public String getLink() {
		return link;
	}

	public int getOccurrence() {
		return occurrence;
	}

	@Override
	public String toString() {
		return "CatalogueType [name=" + name + ", img=" + img + ", link=" + link + ", occurrence=" + occurrence + "]";
	}

	@Override
	public int compareTo(CatalogueType o) {
		if (this.occurrence > o.occurrence)
			return 1;
		if (this.occurrence < o.occurrence)
			return -1;
		return 0;
	}
	
	
}
