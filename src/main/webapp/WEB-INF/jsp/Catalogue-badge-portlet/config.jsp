<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>

<%@ page import="com.liferay.portal.kernel.util.Constants" %>
<%@ page import="com.liferay.portal.kernel.util.GetterUtil" %>
<%@ page import="com.liferay.portal.kernel.util.StringPool" %>
<%@ page import="com.liferay.portal.util.PortalUtil" %>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />
<liferay-portlet:actionURL portletConfiguration="true"
	var="configurationURL" />

<%
String appURL_cfg = GetterUtil.getString(portletPreferences.getValue("catalogueURL", StringPool.BLANK));

String types2Show_cfg = GetterUtil.getString(portletPreferences.getValue("catalogueTypes2ShowNo", StringPool.BLANK));
%>

<aui:form action="<%=configurationURL%>" method="post" name="fm">
	<aui:input name="<%=Constants.CMD%>" type="hidden"
		value="<%=Constants.UPDATE%>" />

	<!-- Application URL -->
	<aui:field-wrapper cssClass="field-group">
		<aui:input style="width: 100%;" name="preferences--catalogueURL--"
			type="text" cssClass="text long-field" showRequiredLabel="false"
			label="Catalogue URL" inlineField="true" inlineLabel="left"
			placeholder="Enter Catalogue URL (e.g. https://ckan-sobigdata.d4science.org)"
			helpMessage="Actual endpoint of the ckan instance (e.g. https://ckan-sobigdata.d4science.org)"
			value="<%=appURL_cfg%>" required="false" />
			
			
		<aui:input style="width: 30%;" name="preferences--catalogueTypes2ShowNo--"
			type="text" cssClass="text long-field" showRequiredLabel="false"
			label="Number of types to show" inlineField="true" inlineLabel="left"
			placeholder="Enter the number of items to show"
			helpMessage="Enter the number of items to show (default is 5)"
			value="<%=types2Show_cfg%>" required="false" />
	</aui:field-wrapper>
	
	<aui:button-row>
		<aui:button type="submit" />
	</aui:button-row>

</aui:form>