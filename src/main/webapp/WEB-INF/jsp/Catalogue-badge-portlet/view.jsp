<%@ page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ page import="com.liferay.portal.kernel.util.Constants"%>
<%@ page import="com.liferay.portal.kernel.util.GetterUtil"%>
<%@ page import="com.liferay.portal.kernel.util.StringPool"%>
<%@ page import="com.liferay.portal.util.PortalUtil"%>


<!-- show the types only when the ckan instance is specified in the config portlet part  -->
<c:choose>
	<c:when test="${not empty catalogueActualURL}">
		<input type="hidden" value="${catalogueURL}" id="catalogueURL">
		<div class="container-fluid">
			<div class="row-fluid">
				<div class="span6">
					<div class="input-append input-catalogue .hidden-phone">
						<input type="text" class="input-large" name="q" value=""
							autocomplete="off" placeholder="Insert keywords here"
							id="inputQueryCatalogue">
						<button class="btn btn-primary" type="button" title="Search"
							id="searchCatalogueButton">
							<i class="icon-search"></i> <span>Search</span>
						</button>
					</div>
					<div>
						<a href="${catalogueURL}?path=/dataset">See All Items</a> <a
							href="${catalogueURL}?path=/types" style="float: right;">See
							All Types</a>
					</div>
				</div>
				<div class="span6">
					<c:forEach var="type" items="${catalogueTypes}">
						<div class="media-item-homepage">
							<a href="${type.link}" title="View ${type.name}"> <img
								class="logo-homepage" src="${type.img}" alt="${type.name}"
								title="${type.name} group">
							</a>
							<p class="media-heading-homepage">
								<a href="${type.link}" title="View ${type.name}">
									${type.name} (${type.occurrence}) </a>
							</p>
						</div>
					</c:forEach>
				</div>
			</div>
		</div>
	</c:when>
	<c:otherwise>
		<div id="catalogueDiv">
			<div class="input-append input-catalogue">
				<input type="text" class="input-large" name="q" value=""
					autocomplete="off" placeholder="Insert keywords here"
					id="inputQueryCatalogue">
				<button class="btn btn-primary" type="button" title="Search"
					id="searchCatalogueButton">
					<i class="icon-search"></i> <span>Search</span>
				</button>
			</div>
			<div class="catstats">
				<ul>
					<li><a href="${catalogueURL}?path=/dataset/"> <b><span>${itemsNo}</span></b>
							items
					</a></li>
					<li><a href="${catalogueURL}?path=/organization/"> <b><span>${organisationsNo}</span></b>
							organisation
					</a></li>
					<li><a href="${catalogueURL}?path=/group/"> <b><span>${groupsNo}</span></b>
							groups
					</a></li>
					<li><a href="${catalogueURL}?path=/type/"> <b><span>${typesNo}</span></b>
							types
					</a></li>
				</ul>
			</div>
		</div>
	</c:otherwise>
</c:choose>

